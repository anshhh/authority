import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Mainapp from '../components/Mainapp.vue';
Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Mainapp",
    component: Mainapp
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
