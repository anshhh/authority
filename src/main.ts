import Vue from "vue";
import {BootstrapVue,BootstrapVueIcons } from 'bootstrap-vue'
import CountryFlag from 'vue-country-flag'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from "./App.vue";

import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'


Vue.config.productionTip = false;
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(PerfectScrollbar)
Vue.component('country-flag', CountryFlag)


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
