const state = { 
     blog:[{
        value: "IP 5",
        country: [{
                value: 'chn',
                text: 'China'
            },
            {
                value: 'jp',
                text: 'Japan'
            },
            {
                value: 'sk',
                text: 'South Korea'
            },
            {
                value: 'us',
                text: 'United States'
            },
            {
                value: 'ind',
                text: 'EPO'
            }
        ]
    },
    {
        value: "Africa",
        country: [{
                value: 'dz',
                text: 'Algeria'
            },
            {
                value: 'eg',
                text: 'Egypt'
            },
            {
                value: 'ken',
                text: 'Kenya'
            },
            {
                value: 'ma',
                text: 'Morocco'
            },
            {
                value: 'mw',
                text: 'Malawi'
            },
            {
                value: 'tn',
                text: 'Tunisia'
            },
            {
                value: 'za',
                text: 'South Africa'
            },
            {
                value: 'zm',
                text: 'Zambia'
            },
            {
                value: 'zw',
                text: 'Zimbabwe'
            }
        ]
    },
    {
        value: "Asia",
        country: [{
                value: 'am',
                text: 'Armenia'
            },
            {
                value: 'ind',
                text: 'India'
            },
            {
                value: 'chn',
                text: 'China'
            },
            {
                value: 'hk',
                text: 'Hong Kong'
            },
            {
                value: 'my',
                text: 'Malaysia'
            }
        ]
    },
    {
        value: "Australia / Oceania",
        country: [{
                value: 'aus',
                text: 'Australia'
            },
            {
                value: 'nz',
                text: 'New Zealand'
            }
        ]
    },
    {
        value: "Europe",
        country: [{
                value: 'at',
                text: 'Austria'
            },
            {
                value: 'be',
                text: 'Belgium'
            },
            {
                value: 'bg',
                text: 'Bulgaria'
            },
            {
                value: 'hn',
                text: 'Hungary'
            },
            {
                value: 'fra',
                text: 'France'
            }
        ]
    },
    {
        value: "North America",
        country: [{
                value: 'can',
                text: 'Canada'
            },
            {
                value: 'cub',
                text: 'Cuba'
            },
            {
                value: 'mex',
                text: 'Mexico'
            },
            {
                value: 'us',
                text: 'United States'
            },
            {
                value: 'pan',
                text: 'Panama'
            }
        ]
    },
    {
        value: "South America",
        country: [{
            value: 'at',
            text: 'Austria'
        },
        {
            value: 'be',
            text: 'Belgium'
        },
        {
            value: 'bg',
            text: 'Bulgaria'
        },
            {
                value: 'us',
                text: 'United States'
            },
            {
                value: 'ind',
                text: 'EPO'
            }
        ]
    },
    {
        value: "Organization",
        country: [{
                value: 'cn',
                text: 'China'
            },
            {
                value: 'jp',
                text: 'Japan'
            },
            {
                value: 'hk',
                text: 'Hong Kong'
            },
            {
                value: 'my',
                text: 'Malaysia'
            }
        ]
    }
]

};

const getters = {
    allData: state => state.blog
  };

// const actions = {
//     async getItems({commit}: any){
//         const response = await axios.get("http://localhost:3000/blog")
//         commit("setItems",response.data);
//     }
// };

const mutations = {
    setItems: (state: any,blog: any) => (state.blog = blog)
};

export default {
  state,
  getters,
  mutations
};